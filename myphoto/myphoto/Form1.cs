﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace myphoto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void mnuLoad_Click(object sender, EventArgs e)
        {
           //taoj trong code
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open Photos";
            dlg.Filter = "jpg files (*.jpg) |*.jpg|All files (*.*)|*.*";
            //mở hộp thoại
            if (dlg.ShowDialog() == DialogResult.OK) 
            {
                // dlg.OpenFile();
                //cach 2
                // dlg.FileName
                //su dung exception
                try
                {
                    pbxPhoto.Image = new Bitmap(dlg.OpenFile());
                    SetStatusStrip(dlg.FileName);
                }
                catch (ArgumentException ex)
                {
                    //bao
                    MessageBox.Show("Unable to load files : " + ex.Message);
                    pbxPhoto.Image = null;
                }
            }
            dlg.Dispose();
            //đọc chương 4

        }

        private void ctxMenuPhoto_Opening(object sender, CancelEventArgs e)
        {
            MenuStrip ms = new MenuStrip();

            ToolStripMenuItem menuTop = new ToolStripMenuItem("Top");

            ms.Items.Add(menuTop);
            ContextMenuStrip ctxMenu = new ContextMenuStrip();

            ctxMenu.Items.Add("Item 1");

            ctxMenu.Items.Add("Item 2");

            ctxMenu.Items.Add("Item 3");

            // Assign context menu as dropdown for the top-level item

            menuTop.DropDown = ctxMenu;
        }

        private void pbxPhoto_Click(object sender, EventArgs e)
        {

        }

        private void ProcessPhoto(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            string enumVal = item.Tag as string;
            if (enumVal != null)
                pbxPhoto.SizeMode = (PictureBoxSizeMode)Enum.Parse(typeof(PictureBoxSizeMode),enumVal);


        }

        private void mnuImage_DropDownOpening(object sender, EventArgs e)
        {
            ToolStripDropDownItem parent = (ToolStripDropDownItem)sender;
            if (parent != null)
            {
                //them tostring.... lay duoi dang ki tu
                string enumVal = pbxPhoto.SizeMode.ToString();
                //duyet qua Image
                foreach (ToolStripMenuItem item in parent.DropDownItems)
                {
                    item.Enabled = (pbxPhoto.Image != null);
                    item.Checked = item.Tag.Equals(enumVal);
                }

                
            }
        }

        private void sttInfo_Click(object sender, EventArgs e)
        {

        }
        private void SetStatusStrip(string path)
{
            if (pbxPhoto.Image != null)
{
sttInfo.Text = path;
                sttImageSize.Text
= String.Format("{0:#}x{1:#}",
pbxPhoto.Image.Width,
pbxPhoto.Image.Height);
// statusAlbumPos is set in ch. 6
}
            else
            {
                sttInfo.Text = null;
                sttImageSize.Text = null;
                sttAlbumPos.Text = null;
            }
}
    }
}
