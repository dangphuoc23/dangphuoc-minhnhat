﻿namespace hello_world
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblhelloworld = new System.Windows.Forms.Label();
            this.btnchao = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblhelloworld
            // 
            this.lblhelloworld.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblhelloworld.AutoSize = true;
            this.lblhelloworld.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblhelloworld.Location = new System.Drawing.Point(115, 55);
            this.lblhelloworld.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblhelloworld.Name = "lblhelloworld";
            this.lblhelloworld.Size = new System.Drawing.Size(169, 32);
            this.lblhelloworld.TabIndex = 0;
            this.lblhelloworld.Text = "Hello World";
            // 
            // btnchao
            // 
            this.btnchao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnchao.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnchao.Location = new System.Drawing.Point(142, 128);
            this.btnchao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnchao.Name = "btnchao";
            this.btnchao.Size = new System.Drawing.Size(112, 30);
            this.btnchao.TabIndex = 1;
            this.btnchao.Text = "Chào";
            this.btnchao.UseVisualStyleBackColor = true;
            this.btnchao.AutoSizeChanged += new System.EventHandler(this.btnchao_Click);
            this.btnchao.Click += new System.EventHandler(this.btnchao_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 336);
            this.Controls.Add(this.btnchao);
            this.Controls.Add(this.lblhelloworld);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblhelloworld;
        private System.Windows.Forms.Button btnchao;
    }
}

