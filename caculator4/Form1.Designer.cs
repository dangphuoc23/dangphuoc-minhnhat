﻿namespace caculator4
{
    partial class form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.lbldisplay = new System.Windows.Forms.TextBox();
            this.btn0 = new System.Windows.Forms.Button();
            this.btncham = new System.Windows.Forms.Button();
            this.btnbang = new System.Windows.Forms.Button();
            this.btncong = new System.Windows.Forms.Button();
            this.btnnhan = new System.Windows.Forms.Button();
            this.btntru = new System.Windows.Forms.Button();
            this.btnchia = new System.Windows.Forms.Button();
            this.btnnho = new System.Windows.Forms.Button();
            this.btnphantram = new System.Windows.Forms.Button();
            this.btnbackspace = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.lblsplay = new System.Windows.Forms.TextBox();
            this.btncan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn7.Location = new System.Drawing.Point(12, 101);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(40, 28);
            this.btn7.TabIndex = 0;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.nhapso);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn8.Location = new System.Drawing.Point(58, 101);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(40, 28);
            this.btn8.TabIndex = 2;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.nhapso);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn9.Location = new System.Drawing.Point(104, 101);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(40, 28);
            this.btn9.TabIndex = 3;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.nhapso);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn4.Location = new System.Drawing.Point(12, 147);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(40, 28);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.nhapso);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn5.Location = new System.Drawing.Point(58, 147);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(40, 28);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.nhapso);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn6.Location = new System.Drawing.Point(104, 147);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(40, 28);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.nhapso);
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn1.Location = new System.Drawing.Point(12, 191);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(40, 28);
            this.btn1.TabIndex = 7;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.nhapso);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn2.Location = new System.Drawing.Point(58, 191);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(40, 28);
            this.btn2.TabIndex = 8;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.nhapso);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn3.Location = new System.Drawing.Point(104, 191);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(40, 28);
            this.btn3.TabIndex = 9;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.nhapso);
            // 
            // lbldisplay
            // 
            this.lbldisplay.Location = new System.Drawing.Point(13, 12);
            this.lbldisplay.Name = "lbldisplay";
            this.lbldisplay.Size = new System.Drawing.Size(218, 20);
            this.lbldisplay.TabIndex = 10;
            this.lbldisplay.Text = "0.";
            this.lbldisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn0.Location = new System.Drawing.Point(58, 225);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(40, 28);
            this.btn0.TabIndex = 11;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.nhapso);
            // 
            // btncham
            // 
            this.btncham.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btncham.Location = new System.Drawing.Point(104, 225);
            this.btncham.Name = "btncham";
            this.btncham.Size = new System.Drawing.Size(40, 28);
            this.btncham.TabIndex = 12;
            this.btncham.Text = ".";
            this.btncham.UseVisualStyleBackColor = true;
            this.btncham.Click += new System.EventHandler(this.nhapso);
            // 
            // btnbang
            // 
            this.btnbang.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnbang.Location = new System.Drawing.Point(207, 195);
            this.btnbang.Name = "btnbang";
            this.btnbang.Size = new System.Drawing.Size(40, 58);
            this.btnbang.TabIndex = 13;
            this.btnbang.Text = "=";
            this.btnbang.UseVisualStyleBackColor = true;
            this.btnbang.Click += new System.EventHandler(this.btnbang_Click);
            // 
            // btncong
            // 
            this.btncong.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btncong.Location = new System.Drawing.Point(161, 101);
            this.btncong.Name = "btncong";
            this.btncong.Size = new System.Drawing.Size(40, 28);
            this.btncong.TabIndex = 14;
            this.btncong.Text = "+";
            this.btncong.UseVisualStyleBackColor = true;
            this.btncong.Click += new System.EventHandler(this.nhappheptoan);
            // 
            // btnnhan
            // 
            this.btnnhan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnnhan.Location = new System.Drawing.Point(161, 191);
            this.btnnhan.Name = "btnnhan";
            this.btnnhan.Size = new System.Drawing.Size(40, 28);
            this.btnnhan.TabIndex = 15;
            this.btnnhan.Text = "x";
            this.btnnhan.UseVisualStyleBackColor = true;
            this.btnnhan.Click += new System.EventHandler(this.nhappheptoan);
            // 
            // btntru
            // 
            this.btntru.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btntru.Location = new System.Drawing.Point(161, 147);
            this.btntru.Name = "btntru";
            this.btntru.Size = new System.Drawing.Size(40, 28);
            this.btntru.TabIndex = 16;
            this.btntru.Text = "-";
            this.btntru.UseVisualStyleBackColor = true;
            this.btntru.Click += new System.EventHandler(this.nhappheptoan);
            // 
            // btnchia
            // 
            this.btnchia.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnchia.Location = new System.Drawing.Point(161, 225);
            this.btnchia.Name = "btnchia";
            this.btnchia.Size = new System.Drawing.Size(40, 28);
            this.btnchia.TabIndex = 17;
            this.btnchia.Text = "/";
            this.btnchia.UseVisualStyleBackColor = true;
            this.btnchia.Click += new System.EventHandler(this.nhappheptoan);
            // 
            // btnnho
            // 
            this.btnnho.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnnho.Location = new System.Drawing.Point(12, 225);
            this.btnnho.Name = "btnnho";
            this.btnnho.Size = new System.Drawing.Size(40, 28);
            this.btnnho.TabIndex = 18;
            this.btnnho.Text = "+/-";
            this.btnnho.UseVisualStyleBackColor = true;
            this.btnnho.Click += new System.EventHandler(this.btnnho_Click);
            // 
            // btnphantram
            // 
            this.btnphantram.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnphantram.Location = new System.Drawing.Point(207, 101);
            this.btnphantram.Name = "btnphantram";
            this.btnphantram.Size = new System.Drawing.Size(40, 28);
            this.btnphantram.TabIndex = 20;
            this.btnphantram.Text = "%";
            this.btnphantram.UseVisualStyleBackColor = true;
            this.btnphantram.Click += new System.EventHandler(this.btnphantram_Click);
            // 
            // btnbackspace
            // 
            this.btnbackspace.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnbackspace.Location = new System.Drawing.Point(59, 55);
            this.btnbackspace.Name = "btnbackspace";
            this.btnbackspace.Size = new System.Drawing.Size(85, 28);
            this.btnbackspace.TabIndex = 21;
            this.btnbackspace.Text = "Backspace";
            this.btnbackspace.UseVisualStyleBackColor = true;
            this.btnbackspace.Click += new System.EventHandler(this.btnbackspace_Click);
            // 
            // btnC
            // 
            this.btnC.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnC.Location = new System.Drawing.Point(13, 55);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(40, 28);
            this.btnC.TabIndex = 22;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // lblsplay
            // 
            this.lblsplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblsplay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblsplay.Location = new System.Drawing.Point(12, 13);
            this.lblsplay.Name = "lblsplay";
            this.lblsplay.Size = new System.Drawing.Size(223, 26);
            this.lblsplay.TabIndex = 23;
            this.lblsplay.Text = "0.";
            this.lblsplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btncan
            // 
            this.btncan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btncan.Location = new System.Drawing.Point(207, 147);
            this.btncan.Name = "btncan";
            this.btncan.Size = new System.Drawing.Size(40, 28);
            this.btncan.TabIndex = 24;
            this.btncan.Text = "√";
            this.btncan.UseVisualStyleBackColor = true;
            this.btncan.Click += new System.EventHandler(this.btncan_Click);
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 261);
            this.Controls.Add(this.btncan);
            this.Controls.Add(this.lblsplay);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnbackspace);
            this.Controls.Add(this.btnphantram);
            this.Controls.Add(this.btnnho);
            this.Controls.Add(this.btnchia);
            this.Controls.Add(this.btntru);
            this.Controls.Add(this.btnnhan);
            this.Controls.Add(this.btncong);
            this.Controls.Add(this.btnbang);
            this.Controls.Add(this.btncham);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(297, 300);
            this.Name = "form1";
            this.Text = "caculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.TextBox lbldisplay;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btncham;
        private System.Windows.Forms.Button btnbang;
        private System.Windows.Forms.Button btncong;
        private System.Windows.Forms.Button btnnhan;
        private System.Windows.Forms.Button btntru;
        private System.Windows.Forms.Button btnchia;
        private System.Windows.Forms.Button btnnho;
        private System.Windows.Forms.Button btnphantram;
        private System.Windows.Forms.Button btnbackspace;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.TextBox lblsplay;
        private System.Windows.Forms.Button btncan;
    }
}

