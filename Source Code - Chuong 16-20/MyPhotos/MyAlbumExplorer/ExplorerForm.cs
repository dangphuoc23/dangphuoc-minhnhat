﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Manning.MyPhotoAlbum;
using Manning.MyPhotoControls;

namespace MyAlbumExplorer
{
    public partial class ExplorerForm : Form
    {
        public ExplorerForm()
        {
            InitializeComponent();
        }
        private Photograph currentPhoto = null;
        private void atvAlbumTree_BeforeSelect(
object sender,
TreeViewCancelEventArgs e)
{
if (currentPhoto != null)
{
spbxPhoto.Image = null;
currentPhoto.ReleaseImage();
currentPhoto = null;
}
}

        private void ExplorerForm_Load(object sender, EventArgs e)
        {
            // Note: picture box cannot receive focus
if (lvAlbumList.Focused)
DisplayListItemProperties();
else if (atvAlbumTree.Focused)
DisplayTreeNodeProperties();
        }
        private void DisplayTreeNodeProperties()
{
TreeNode node = atvAlbumTree.SelectedNode;
if (node is AlbumNode)
{
AlbumNode aNode = (AlbumNode) node;
AlbumManager mgr = aNode.GetManager(true);
if (mgr != null)
DisplayAlbumProperties(mgr);
}
else if (node is PhotoNode)
{
PhotoNode pNode = (PhotoNode)node;
DisplayPhotoProperties(pNode.Photograph);
}
            // Preserve and display any changes
atvAlbumTree.SaveAlbumChanges();
atvAlbumTree.RefreshNode();
}
        private void DisplayListItemProperties()
{
if (lvAlbumList.SelectedItems.Count == 1)
{
ListViewItem item
= lvAlbumList.SelectedItems[0];
if (item.Tag is AlbumManager)
{
AlbumManager mgr = (AlbumManager)item.Tag;
DisplayAlbumProperties(mgr);
AssignSubItems(item, mgr.FullName, mgr);
if (mgr.Album.HasChanged)
mgr.Save();
}else if (item.Tag is Photograph)
{
Photograph photo = (Photograph)item.Tag;
DisplayPhotoProperties(photo);
if (photo.HasChanged)
{
AssignSubItems(item, photo);
atvAlbumTree.SaveAlbumChanges();
}
}
else
MessageBox.Show("The properties for this"
+ " item cannot be displayed.");
}
}
        private void DisplayAlbumProperties(
AlbumManager mgr)
{
using (AlbumEditDialog dlg
= new AlbumEditDialog(mgr))
{
dlg.ShowDialog();
}
}
        private void DisplayPhotoProperties(
Photograph photo)
{
using (PhotoEditDialog dlg
= new PhotoEditDialog(photo))
{
dlg.ShowDialog();
}
}
            private MyListViewComparer _comparer;
private MyListViewComparer MyComparer
{ get { return comparer; } }

        private void mnuEDitProperties_Click(object sender, EventArgs e)
        {
        
        }

        private void mnuEditLabel_DropDownOpening(object sender, EventArgs e)
        {
            mnuEditLabel.Enabled = true ;
            if (lvAlbumList.Focused)
                mnuEditLabel.Text = "Captio&n";
else if (atvAlbumTree.Focused)
                mnuEditLabel .Text = "&Name";
else
                mnuEditLabel .Enabled = false;
        }
        private void menuEditLabel_Click
(object sender, EventArgs e)
{
            if (lvAlbumList.Focused)
{
if (lvAlbumList.FocusedItem != null)
lvAlbumList.FocusedItem.BeginEdit();
}
else if (atvAlbumTree.Focused)
{
if (atvAlbumTree.SelectedNode != null)
atvAlbumTree.SelectedNode.BeginEdit();
}
}
        private void lvAlbumList_KeyDown(
object sender, KeyEventArgs e)
{
if (e.KeyCode == Keys.F2)
{
if (lvAlbumList.FocusedItem != null)
lvAlbumList.FocusedItem.BeginEdit();
e.Handled = true;
        }}
        private void lvAlbumList AfterLabelEdit
(object sender, LabelEditEventArgs e)
{
            if (String.IsNullOrEmpty(e.Label))
{
e.CancelEdit = true;
return;
}
            ListViewItem item = lvAlbumList.Items[e.Item];
            if (item.Tag is Photograph)
{
Photograph photo = (Photograph)item.Tag;
photo.Caption = e.Label;
if (photo.HasChanged)
atvAlbumTree.SaveAlbumChanges();
}
            else
{
RenameAlbum(item, e.Label);
}
}
        private void RenameAlbum(
ListViewItem item, string newName)
{
try
{
string oldPath = null;
string newPath = null;
    if (item.Tag is AlbumManager)
{
AlbumManager mgr = (AlbumManager)item.Tag;
oldPath = mgr.FullName;
mgr.RenameAlbum(newName);
newPath = mgr.FullName;
}
    else if (item.Tag is string)
{
// Presume tag is album path
oldPath = (string)item.Tag;
newPath = AlbumManager.RenameAlbum(
oldPath, newName);
item.Tag = newPath;
}
    if (oldPath != null)
{
// Update the album node
AlbumNode aNode
= atvAlbumTree.FindAlbumNode(oldPath);
if (aNode != null)
aNode.UpdatePath(newPath);
}
}
catch (ArgumentException aex)
{
MessageBox.Show("Unable to rename album. ["
+ aex.Message + "]");
}
}
        private void lvAlbumList ItemActivate(
object sender, EventArgs e)
{// ListViewItem is visible and selected
ListViewItem item
= lvAlbumList.SelectedItems[0];
            TreeNode node = null;
if (ShowingAlbums)
{
string albumPath;
AlbumManager mgr = item.Tag as AlbumManager;
if (mgr == null)
albumPath = item.Tag as string;
else
albumPath = mgr.FullName;
if (albumPath != null)
node = atvAlbumTree.
FindAlbumNode(albumPath);
}
else
{
Photograph photo = item.Tag as Photograph;
if (photo != null)
node = atvAlbumTree.FindPhotoNode(photo);
}
            if (node != null)
atvAlbumTree.SelectedNode = node;
}
            
        protected override void OnLoad(EventArgs e)
        {
   
            // Assign title bar
            Version v = new Version(Application.
            ProductVersion);
            this.Text = String.Format(
            "MyAlbumExplorer {0:#}.{1:#}",
            v.Major, v.Minor);
    comparer = new MyListViewComparer(
lvAlbumList);
lvAlbumList.ListViewItemSorter = MyComparer;
            base.OnLoad(e);

        }
        private void atvAlbumTree AfterSelect(
object sender, TreeViewEventArgs e)
{
if (e.Node is PhotoNode)
DisplayPhoto(e.Node as PhotoNode);
}
        private void DisplayPhoto(
PhotoNode photoNode)
{
currentPhoto = photoNode.Photograph;
spbxPhoto.Image = currentPhoto.Image;
}

        private void albumTree_AfterSelect(object sender, TreeViewEventArgs e)
        { 
            try
{
lvAlbumList.BeginUpdate();
lvAlbumList.Clear();
            if (e.Node is AlbumDirectoryNode)
ListAlbumData(e.Node
as AlbumDirectoryNode);
else if (e.Node is AlbumNode)
ListPhotoData(e.Node as AlbumNode);
else if (e.Node is PhotoNode)
DisplayPhoto(e.Node as PhotoNode);
else
throw new ArgumentException(
"Unrecognized node");
            }
finally
{
lvAlbumList.EndUpdate();
}
}

        }

        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mnuView_DropDownOpened(object sender, EventArgs e)
        {
            View v = lvAlbumList.View;
            menuViewLarge.Checked = (v == View.LargeIcon);
            menuViewSmall.Checked = (v == View.SmallIcon);
            menuViewList.Checked = (v == View.List);
            menuViewDetails.Checked = (v == View.Details);
            menuViewTiles.Checked = (v == View.Tile);
        }
        private void menuView_DropDownItemClicked(
            object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            string enumVal = item.Tag as string;
            if (enumVal != null)
            {
                lvAlbumList.View = (View)
                Enum.Parse(typeof(View), enumVal);
            }
    private void atvAlbumTree AfterSelect(object sender, TreeViewEventArgs e)
{
    try
{
bool oldShowingAlbums = ShowingAlbums;
lvAlbumList.BeginUpdate();
lvAlbumList.Clear();
    if (e.Node is AlbumDirectoryNode)
ListAlbumData(e.Node
as AlbumDirectoryNode);
else if (e.Node is AlbumNode)
ListPhotoData(e.Node as AlbumNode);
else if (e.Node is PhotoNode)
DisplayPhoto(e.Node as PhotoNode);
else
throw new ArgumentException(
"Unrecognized node");
}
if (lvAlbumList.Visible
&& ShowingAlbums != oldShowingAlbums)
{
// Columns changed, so reset sorting
MyComparer.SortColumn = 0;
lvAlbumList.Sorting = SortOrder.Ascending;
}
}
finally
{
lvAlbumList.EndUpdate();
}

private void DisplayPhoto(PhotoNode photoNode)
{
spbxPhoto.Visible = true;
lvAlbumList.Visible = false;
}
private bool showingAlbums = true;
private bool ShowingAlbums
{
get { return showingAlbums; }
set { showingAlbums = value; }
}
private void ListAlbumData(
AlbumDirectoryNode dirNode)
{
// Show albums in list view
spbxPhoto.Visible = false;
lvAlbumList.Visible = true;
ShowingAlbums = true;// Add the albums for given node
foreach (AlbumNode aNode
in dirNode.AlbumNodes)
{string text
= Path.GetFileNameWithoutExtension(
aNode.AlbumPath);
ListViewItem item
= lvAlbumList.Items.Add(text);
item.ImageKey = aNode.ImageKey;
    }
}
private void ListPhotoData(AlbumNode albumNode)
{
// Show photographs in list view
spbxPhoto.Visible = false;
lvAlbumList.Visible = true;
ShowingAlbums = false;
ListViewItem item = lvAlbumList.Items.Add(text);
ListViewItem item = new ListViewItem(p.Caption, "Photo");
lvAlbumList.Items.Add(item);
// Add the photos for given node
AlbumManager mgr= albumNode.GetManager(true);
if (mgr != null)
{
foreach (Photograph p in mgr.Album)
{
ListViewItem item
= new ListViewItem(p.Caption, "Photo");
lvAlbumList.Items.Add(item);
} 
private void lvAlbumList ColumnClick(
object sender, ColumnClickEventArgs e)
{
if (e.Column == MyComparer.SortColumn)
{
// Switch the sort order for this column
if (lvAlbumList.Sorting
== SortOrder.Ascending)
lvAlbumList.Sorting = SortOrder.Descending;
else
lvAlbumList.Sorting = SortOrder.Ascending;
}
else
{
// Define new sort column and order
MyComparer.SortColumn = e.Column;
lvAlbumList.Sorting = SortOrder.Ascending;
}
lvAlbumList.Sort();
}
}
}