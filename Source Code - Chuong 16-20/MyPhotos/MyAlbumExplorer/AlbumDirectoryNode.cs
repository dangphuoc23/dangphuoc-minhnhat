﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace MyAlbumExplorer
{
    internal class AlbumDirectoryNode : TreeNode
    {
        private string _albumDir;
        public string AlbumDirectory
        { get { return _albumDir; } }
        public AlbumDirectoryNode(string name,
string albumDir)
            : base(name)
        {
            if (albumDir == null)
                throw new ArgumentNullException("albumDir");
            if (!Directory.Exists(albumDir))
                throw new ArgumentException(
                "albumDir is not a valid directory");
            albumDir = albumDir;
            this.Nodes.Add("child");
            this.ImageKey = "AlbumDir";
            this.SelectedImageKey = "AlbumDir";
        }
            public string[] AlbumFiles
{
get { return Directory.GetFiles(
AlbumDirectory, "*.abm"); }
}
        private AlbumNode[] albumNodes = null;
public AlbumNode[] AlbumNodes
{
get
{
CreateAlbumNodes();
return albumNodes;
}
}
        public void CreateAlbumNodes()
{
string[] files = AlbumFiles;
int count = files.Length;
            if (albumNodes == null
|| albumNodes.Length != count)
{
Nodes.Clear();
albumNodes = new AlbumNode[count];
for (int i = 0; i < count; i++)
{
// Add album node
string s = files[i];
string name
= Path.GetFileNameWithoutExtension(s);
albumNodes[i] = new AlbumNode(name, s);
}
Nodes.AddRange(albumNodes);
    }
}
    }
}
