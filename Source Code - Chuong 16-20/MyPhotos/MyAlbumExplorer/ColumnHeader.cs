﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyAlbumExplorer
{
    class ColumnHeader
    {
        private void ListAlbumData(. . .)
{
// Show albums in list view
. . .
// Presume list cleared, so recreate columns
lvAlbumList.Columns.Add("Name", 80);
lvAlbumList.Columns.Add("Title", 120);
lvAlbumList.Columns.Add("Size", 40,
HorizontalAlignment.Center);
// Add the albums for given node
. . .
    }
    private void ListPhotoData(AlbumNode albumNode)
{
// Show photographs in list view
. . .
// Presume contents cleared, so recreate columns
lvAlbumList.Columns.Add("Caption", 120);
lvAlbumList.Columns.Add("Photographer", 100);
lvAlbumList.Columns.Add("Taken", 80,
HorizontalAlignment.Center);
lvAlbumList.Columns.Add("File Name", 200);
// Add the photos for given node
. . .
}
}
