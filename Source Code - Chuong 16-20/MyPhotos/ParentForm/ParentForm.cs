﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Manning.MyPhotoAlbum;
using Manning.MyPhotoControls;


namespace ParentForm
{
    public partial class ParentForm : Form
    {
        public ParentForm()
        {
            InitializeComponent();
        }

        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            CreateMdiChild(new Form());
        }
        private void CreateMdiChild(
            Form child)
        {
            child.MdiParent = this;
            child.Show();
        }

        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            OpenAlbum();
        }
        private void OpenAlbum()
{
string path = null;
string pwd = null;
if (AlbumController.OpenAlbumDialog(
ref path, ref pwd))
{
try
{
CreateMdiChild(new MainForm(path, pwd));
}
catch (AlbumStorageException aex)
{
MessageBox.Show(this,
"Unable to open album " + path
+ "\n [" + aex.Message + "]",
"Open Album Error",
MessageBoxButtons.OK,
MessageBoxIcon.Error);
    }
   }
 }

        private void mnuFileClose_Click(object sender, EventArgs e)
        {
        if (ActiveMdiChild != null)
ActiveMdiChild.Close();
        }

        private void mnuFile_DropDownOpening(object sender, EventArgs e)
        {
        mnuFileClose.Enabled
= (this.ActiveMdiChild != null);
        }
        public MainForm (string path, string pwd);
             this();
    {
        // Caller must deal with any exception
Manager = new AlbumManager(path, pwd);

        private void tsbNew_Click(object sender, EventArgs e)
        {
        CreateMdiChild(new MainForm());
        }

        private void tsbOpen_Click(object sender, EventArgs e)
        {
        OpenAlbum();
        }
}}
protected override void OnLoad(
EventArgs e)
{
ComponentResourceManager resources
= new ComponentResourceManager(
typeof(MainForm));
Image newImage = (Image) resources.
GetObject("menuFileNew.Image");
Image openImage = (Image) resources.
GetObject("menuFileOpen.Image");
mnuFileNew.Image = newImage;
mnuFileOpen.Image = openImage;
tsbNew.Image = newImage;
tsbOpen.Image = openImage;
// Adjust form if MDI child
if (this.IsMdiChild)
{
menuStrip1.Visible = false;
toolStripMain.Visible = false;
DisplayAlbum();// Assign MDI parent for all pixel forms
PixelDialog.GlobalMdiParent = this;
}
base.OnLoad(e);
}
internal ToolStrip MainToolStrip
{
get { return toolStripMain; }
}
protected override void
OnMdiChildActivate(EventArgs e)
{
ToolStripManager.RevertMerge(
toolStripParent);
MainForm f = ActiveMdiChild as MainForm;
if (f != null)
{
ToolStripManager.Merge(
f.MainToolStrip,
toolStripParent.Name);
toolStripParent.ImageList
= f.MainToolStrip.ImageList;
}
base.OnMdiChildActivate(e);
}
